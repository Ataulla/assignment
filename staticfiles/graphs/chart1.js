$(document).ready(function() {
    fetchData();

    function fetchData() {
        $.get('chart1').then(function(res) {

            drawChart(res.data);

        });
    }
    function drawChart(data) {
        var result1=[];
        var result2=[];
        var result3=[];
        var result4=[];
        for (var i=0,l=data.manufacturing.length;i<l;i++) result1.push(+data.manufacturing[i]);
        for (var i=0,l=data.sales_distribution.length;i<l;i++) result2.push(+data.sales_distribution[i]);
        for (var i=0,l=data.project_development.length;i<l;i++) result3.push(+data.project_development[i]);
        for (var i=0,l=data.other.length;i<l;i++) result4.push(+data.other[i]);
        Highcharts.chart('container', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Historic World Population by Region'
            },
            subtitle: {
                text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
            },
            xAxis: {
                categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Population (millions)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' millions'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Year 1800',
                data: result1
            }, {
                name: 'Year 1900',
                data: result2
            }, {
                name: 'Year 2000',
                data: result3
            }, {
                name: 'Year 2016',
                data: result4
            }]
        });

    }

});
