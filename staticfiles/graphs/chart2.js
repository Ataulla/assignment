$(document).ready(function() {

    fetchData()
    function fetchData() {

        $.get('chart2').then(function(res) {

            RevenueBarChart(res.data);

        });
    }
    function RevenueBarChart(data) {
        var result=[];
        var result1=[];
        var result2=[];
        var result3=[];
        var result4=[];
        for (var i=0,l=data.installation.length;i<l;i++) result.push(+data.installation[i]);
        for (var i=0,l=data.manufacturing.length;i<l;i++) result1.push(+data.manufacturing[i]);
        for (var i=0,l=data.sales_distribution.length;i<l;i++) result2.push(+data.sales_distribution[i]);
        for (var i=0,l=data.project_development.length;i<l;i++) result3.push(+data.project_development[i]);
        for (var i=0,l=data.other.length;i<l;i++) result4.push(+data.other[i]);
        Highcharts.chart('container1', {
            title: {
                text: 'Solar Employment Growth by Sector, 2010-2016'
            },

            subtitle: {
                text: 'Source: thesolarfoundation.com'
            },

            yAxis: {
                title: {
                    text: 'Number of Employees'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: 2010
                }
            },


            series: [{
                name: 'Installation',
                data: result
            }, {
                name: 'Manufacturing',
                data: result1
            }, {
                name: 'Sales & Distribution',
                data: result2
            }, {
                name: 'Project Development',
                data: result3
            }, {
                name: 'Other',
                data: result4
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });

    }

});
