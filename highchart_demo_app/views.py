from django.shortcuts import render
from django.db import connection
from django.http import JsonResponse

from highchart_demo_app.query import querys
import pandas as pd


def getlowerdf(df):
    """
    function for fetch and manage columns
    :param df:
    :return:
    """
    cols = df.columns
    cols_dict = {}
    for item in cols:
        cols_dict[item] = item.lower()
    df.rename(columns=cols_dict, inplace=True)
    return df


def getdata(qry, todict=False, single=False):
    """
    function for fetch querys and stabilised connection with datatables
    :param qry:
    :param todict:
    :param single:
    :return:
    """
    data = pd.read_sql(qry, connection)
    # connection.close()
    data = getlowerdf(data)
    data = data.fillna(0)
    if todict:
        data = data.to_dict(orient="records")
        if single:
            data = data[0]
    return data


def dashboard(request):

    return render(request, 'index.html',)


def chart1(request):
    """
    :param request:
    :return:
    """
    if request.method == 'GET':

        qry = querys("query")
        df = getdata(qry)

        return JsonResponse({'data':df.to_dict(orient="list")})
    return JsonResponse({'message': 'error'})


def chart2(request):
    """
    :param request:
    :return:
    """
    if request.method == 'GET':
        qry = querys("query")
        df = getdata(qry)

        return JsonResponse({'data': df.to_dict(orient="list")})
    return JsonResponse({'message': 'error'})
