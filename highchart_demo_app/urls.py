from django.urls import path
from highchart_demo_app import views

urlpatterns = [
    path('dashboard/', views.dashboard, name='dashboard'),
    path('chart2', views.chart2, name='chart2'),
    path('chart1', views.chart1, name='chart1'),
]
