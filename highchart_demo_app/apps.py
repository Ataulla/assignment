from django.apps import AppConfig


class HighchartDemoAppConfig(AppConfig):
    name = 'highchart_demo_app'
